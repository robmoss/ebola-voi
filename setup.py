#!/usr/bin/env python

from setuptools import setup, find_packages
import sys

import pybola


setup(
    name=pybola.__package_name__,
    version=pybola.__version__,
    description='Stochastic meta-population model of Ebola transmission',
    license='BSD 3-Clause License',
    author='Rob Moss',
    author_email='rgmoss@unimelb.edu.au',
    packages=find_packages(),
    install_requires=[
        'numpy >= 1.8',
        'scipy >= 0.14',
        'h5py >= 2.0',
    ],
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Topic :: Scientific/Engineering :: Mathematics'
    ],
    test_suite='pybola/test',
    setup_requires=["pytest-runner"],
    tests_require=['pytest'],
)
