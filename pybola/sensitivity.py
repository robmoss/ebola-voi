import itertools


def param_sets(vals_dict):
    keys, vals = zip(*[(k, vs) for (k, vs) in vals_dict.items()])
    return [{k: v for (k, v) in zip(keys, vs)} for vs in itertools.product(*vals)]
