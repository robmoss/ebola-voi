def overrides(intervention, preemptive):
    pars = {'preemptive': preemptive,
            'intervention': intervention.capitalize()}

    if preemptive:
        pars['pr_detect_int'] = 0.65
        pars['detect_nth'] = 10
    else:
        pars['pr_detect_int'] = 0.3
        pars['detect_nth'] = 100

    if intervention == 'none':
        pass
    elif intervention == 'quick':
        pars['extra_delay'] = 14
        pars['extra_ppe'] = 1000
        pars['extra_cap_bed'] = 100
        pars['extra_cap_hcw'] = 100
        pars['extra_cap_ct'] = 0
    elif intervention == 'slow':
        pars['extra_delay'] = 56
        pars['extra_ppe'] = 10000
        pars['extra_cap_bed'] = 1000
        pars['extra_cap_hcw'] = 1000
        pars['extra_cap_ct'] = 0
    else:
        raise ValueError('invalid intervention "{}"'.format(intervention))

    return pars
