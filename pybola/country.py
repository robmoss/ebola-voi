import numpy as np

import pybola.model


def mix_png():
    """The mixing matrix for Papua New Guinea, under the assumption of the
    following numbers of daily journeys between regions:

        Depart        Arrive        Journeys
        ---------------------------------------
        Momase        Highlands     2575.657143
        Port Moresby  Highlands     583.2
        Southern      Highlands     1028.8
        Momase        Islands       142
        Port Moresby  Islands       444.2571429
        Southern      Islands       33.02857143
        Highlands     Momase        2563.314286
        Islands       Momase        140.9714286
        Port Moresby  Momase        929.6857143
        Southern      Momase        777.7714286
        Highlands     Port Moresby  607.8857143
        Islands       Port Moresby  438.4857143
        Momase        Port Moresby  916.3142857
        Southern      Port Moresby  18537.11429
        Highlands     Southern      1028.8
        Islands       Southern      32.91428571
        Momase        Southern      777.7714286
        Port Moresby  Southern      18542.37143

    Note: each column represents the daily rate of travel from one patch P to
    each of the other patches, expressed as a fraction of the population of P.

    Patch populations:
        1. Port Moresby (urban): 365,000
        2. Highlands (rural): 2,854,874
        3. Islands (rural): 741,538
        4. Momase (rural): 1,867,657
        5. Southern (rural): 1,091,250
    """
    return np.array([[0.94383, 0.00021, 0.00059, 0.00049, 0.01699],
                     [0.00160, 0.99845, 0.0,     0.00138, 0.00094],
                     [0.00122, 0.0,     0.99918, 0.00008, 0.00003],
                     [0.00255, 0.00098, 0.00019, 0.99763, 0.00071],
                     [0.05080, 0.00036, 0.00004, 0.00042, 0.98133]])


def png(seed_patch=0, seed=42, override=None):
    """Return simulation and model parmeters for PNG scenarios.

    :param seed_patch: The patch in which to seed the initial exposure.
    :param mixing: The inter-patch mixing matrix (default is :math:`I`).
    :param override: A dictionary of parameter values to override.
    """
    N = np.array([3.650e5, 2.855e6, 7.415e5, 1.868e6, 1.091e6])
    if seed_patch >= len(N):
        raise ValueError("Invalid seed_patch = {}".format(seed_patch))
    mixing = mix_png()
    base = {
        # Identify the country.
        'country': 'Papua New Guinea',
        # Pre-existing healthcare infrastructure.
        'frac_hcw': np.array([0.00195, 0.00080, 0.00090, 0.00036, 0.00026]),
        'max_bed': np.array([60, 193, 56, 57, 24]),
        'max_ct': np.array([15, 50, 14, 15, 6]),
        # Elevated risk in urban areas, reduced risk in rural areas.
        'beta_Ic_c': np.array([0.30, 0.10, 0.10, 0.10, 0.10]),
        'patch_names': np.array(['Port Moresby'.encode("utf-8"),
                                 'Highlands'.encode("utf-8"),
                                 'Islands'.encode("utf-8"),
                                 'Momase'.encode("utf-8"),
                                 'Southern'.encode("utf-8")]),
    }
    if override is not None:
        for k in override:
            if k in base:
                print("Warning: overriding base parameter {}".format(k))
            base[k] = override[k]

    return pybola.model.params(N, len(N), seed_patch, mixing, seed, base)
