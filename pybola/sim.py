import h5py
import numpy as np

import pybola.model
from pybola.model import C


def output_table(sv):
    """Convert multi-dimensional state vector matrices into flat (2D) tables.
    """
    n_steps, n_sv, n_c, n_p = sv.shape
    sv_table = np.empty(
        n_steps * n_sv * n_p,
        dtype=[
            ('step', np.int32), ('model', np.int32), ('patch', np.int32),
            ('Sc', np.int32), ('Sh', np.int32), ('St', np.int32),
            ('Ec', np.int32), ('Eh', np.int32), ('Et', np.int32),
            ('I0c', np.int32), ('I0h', np.int32), ('I0t', np.int32),
            ('Ic', np.int32), ('Ih', np.int32),
            ('Qc', np.int32), ('Qh', np.int32),
            ('Hc', np.int32), ('Hh', np.int32),
            ('Dc', np.int32), ('Dh', np.int32),
            ('Rc', np.int32), ('Rh', np.int32),
            ('Bc', np.int32), ('Bh', np.int32),
            ('PPE', np.int32), ('MaxBed', np.int32), ('MaxCT', np.int32),
            ('ToFD', np.int32), ('ToInt', np.int32),
            ('CumI', np.int32), ('CumQ', np.int32), ('CumH', np.int32),
            ('CumCA', np.int32), ('CumCT', np.int32), ('CumID', np.int32)])
    # Fill the columns one by one.
    sv_table['step'] = np.repeat(np.arange(n_steps), n_sv * n_p)
    sv_table['model'] = np.repeat(np.tile(np.arange(n_sv), n_steps), n_p)
    sv_table['patch'] = np.tile(np.arange(n_p), n_steps * n_sv)
    sv_table['Sc'] = sv[..., C.Sc, :].flatten()
    sv_table['Sh'] = sv[..., C.Sh, :].flatten()
    sv_table['St'] = sv[..., C.St, :].flatten()
    sv_table['Ec'] = sv[..., C.Ec, :].flatten()
    sv_table['Eh'] = sv[..., C.Eh, :].flatten()
    sv_table['Et'] = sv[..., C.Et, :].flatten()
    sv_table['I0c'] = sv[..., C.I0c, :].flatten()
    sv_table['I0h'] = sv[..., C.I0h, :].flatten()
    sv_table['I0t'] = sv[..., C.I0t, :].flatten()
    sv_table['Ic'] = sv[..., C.Ic, :].flatten()
    sv_table['Ih'] = sv[..., C.Ih, :].flatten()
    sv_table['Qc'] = sv[..., C.Qc, :].flatten()
    sv_table['Qh'] = sv[..., C.Qh, :].flatten()
    sv_table['Hc'] = sv[..., C.Hc, :].flatten()
    sv_table['Hh'] = sv[..., C.Hh, :].flatten()
    sv_table['Dc'] = sv[..., C.Dc, :].flatten()
    sv_table['Dh'] = sv[..., C.Dh, :].flatten()
    sv_table['Rc'] = sv[..., C.Rc, :].flatten()
    sv_table['Rh'] = sv[..., C.Rh, :].flatten()
    sv_table['Bc'] = sv[..., C.Bc, :].flatten()
    sv_table['Bh'] = sv[..., C.Bh, :].flatten()
    sv_table['PPE'] = sv[..., C.PPE, :].flatten()
    sv_table['MaxBed'] = sv[..., C.MaxBed, :].flatten()
    sv_table['MaxCT'] = sv[..., C.MaxCT, :].flatten()
    sv_table['ToFD'] = sv[..., C.ToFD, :].flatten()
    sv_table['ToInt'] = sv[..., C.ToInt, :].flatten()
    sv_table['CumI'] = sv[..., C.CumI, :].flatten()
    sv_table['CumQ'] = sv[..., C.CumQ, :].flatten()
    sv_table['CumH'] = sv[..., C.CumH, :].flatten()
    sv_table['CumCA'] = sv[..., C.CumCA, :].flatten()
    sv_table['CumCT'] = sv[..., C.CumCT, :].flatten()
    sv_table['CumID'] = sv[..., C.CumID, :].flatten()
    return sv_table


def run(params):
    """Run a set of simulations and return the model outputs.
    """
    # Create the state vectors and simulate the scenario.
    sv = pybola.model.init(params)
    t = params['time_0']
    dt = params['time_dt']
    steps = int(np.ceil((params['time_1'] - t) / dt))
    for i in range(steps):
        t += dt
        pybola.model.step(params, sv, t, i)
    return output_table(sv)


def outcomes(params, sv_table):
    """Return the fraction of simulations in which the epidemic died out, the
    fraction in which it was controlled, and the fraction in which it was
    uncontrolled (as a tuple)."""
    # Determine the index of the first row for the final time-step.
    from_ix = - params['models'] * params['patches']

    # Calculate the final attack rates
    last_row = sv_table[from_ix:]
    num_pat = params['patches']
    epi_die = 0
    epi_sml = 0
    epi_lrg = 0
    for m in range(params['models']):
        mrows = last_row[m * num_pat:(m + 1) * num_pat]
        net_cases = np.sum(mrows['CumI'])
        final_ar = net_cases / np.sum(params['N'])
        if net_cases < 10:
            epi_die += 1
        elif final_ar <= 0.01:
            epi_sml += 1
        else:
            epi_lrg += 1

    epi_die = epi_die / float(params['models'])
    epi_sml = epi_sml / float(params['models'])
    epi_lrg = epi_lrg / float(params['models'])

    return (epi_die, epi_sml, epi_lrg)


def save_table(filename, params, sv_table):
    """Save the model output to an external file.
    """
    # Determine the index of the first row for the final time-step.
    from_ix = - params['models'] * params['patches']

    # Attempt to avoid tracking times (not supported by h5py < 2.2).
    kwargs = {'track_times': False}

    with h5py.File(filename, 'w') as f:
        # Record whether tracking times have been disabled.
        try:
            f.create_dataset('hdf5_track_times', data=False, **kwargs)
        except TypeError:
            # Cannot disable tracking times (h5py < 2.2).
            kwargs = {}
            f.create_dataset('hdf5_track_times', data=True, **kwargs)

        # Save the simulation parametes.
        pg = f.create_group('params')
        pch_g = pg.create_group('patches')
        sim_g = pg.create_group('simulation')
        for k, v in params.items():
            if isinstance(v, np.ndarray):
                pch_g.create_dataset(k, data=v, **kwargs)
            elif isinstance(v, float):
                sim_g.create_dataset(k, data=v, **kwargs)
            elif isinstance(v, int):
                sim_g.create_dataset(k, data=np.int32(v), **kwargs)
            elif isinstance(v, str):
                sim_g.create_dataset(k, data=v, **kwargs)
            else:
                # Ignore, e.g., the PRNG object.
                if k != 'rnd':
                    print("Ignoring {} with type {}".format(k, type(v)))

        # Compress and checksum the (large) data tables.
        kwargs['compression'] = 'gzip'
        kwargs['shuffle'] = True
        kwargs['fletcher32'] = True

        out_g = f.create_group('output')
        # Save the full time-series for each model instance.
        out_g.create_dataset('all', data=sv_table, **kwargs)
        # Save the final state for each model instance.
        out_g.create_dataset('final', data=sv_table[from_ix:], **kwargs)
