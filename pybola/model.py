"""Multi-patch compartment model of disease transmission."""

from __future__ import division, print_function

import numpy as np
import scipy.stats as sp


def params(N, patches=1, seed_patch=0, mixing=None, seed=42, override=None):
    """The default simulation parameters.

    :param N: The size of the population (either a scalar or an array of
        per-patch population sizes).
    :param patches: The number of model patches.
    :param seed_patch: The patch in which to seed the initial exposure.
    :param mixing: The inter-patch mixing matrix (default is :math:`I`).
    :param seed: The RNG seed (default is ``42``).
    :param override: A dictionary of parameter values to override.
    """
    patch_params = {
        # Population demographics and mixing.
        'N': N,
        'frac_hcw': 0.01,  # Healthcare workers are 1% of the population.
        'eta': 1.0,  # Uniform population mixing: (S/N)^eta.

        # Incubation and infectious periods.
        'sigma': 1 / 10,  # Incubation period of 10 days.
        'gamma': 1 / 10,  # Infectious period of 10 days.
        'gamma0_c': 1 / 4,  # Pre-detection period of 4 days (general).
        'gamma0_h': 1 / 4,  # Pre-detection period of 4 days (HCWs).

        # Force of infection.
        'beta_I0_c': 0.10,  # Low initial infectiousness.
        'beta_I0_h': 0.10,  # Low initial infectiousness.
        'beta_Ic_c': 0.20,  # High subsequent infectiousness.
        'beta_Ic_h': 0.30,  # Elevated risk for healthcare workers.
        'beta_Q_c': 0.05,  # Reduced force due to home-base isolation.
        'beta_Q_h': 0.05,  # Reduced force due to home-base isolation.
        'beta_H_c': 0.00,  # No infections due to hospital isolation.
        'beta_H_h': 0.02,  # Reduced infections due to hospital isolation.
        'beta_R_c': 0.00,  # No infections from recovered individuals.
        'beta_R_h': 0.00,  # No infections from recovered individuals.
        'beta_D_c': 0.20,  # Infectious force of unburied corpses.
        'beta_D_h': 0.20,  # Infectious force of unburied corpses.

        # Recovery and burial rates.
        'phi': 1 / 49,  # Complete recovery in 7 weeks.
        'tau': 1 / 3,  # Pre-burial period of 3 days in the community.

        # Healthcare and intervention parameters.
        'pr_detect': 0,  # Deterministic detection of first case.
        'detect_nth': -1,  # Deterministic detection of case N.
        'pr_detect_int': 0.1,  # Probabilistic detection after first case.
        'max_bed': 0,  # Initial isolation-bed capacity.
        'max_ct': 0,  # Initial contact tracing/monitoring capacity.
        'ppe_stock': 0,  # Initial personal protective equipment stockpile.
        'reduce_beta_I': 1.0,  # Reduce dead-body infectious force.
        'reduce_beta_D': 0.5,  # Reduce dead-body infectious force.
        'reduce_delay': 0,  # Delay in social interventions.
        'extra_cap_bed': 0,  # Extra isolation-bed capacity.
        'extra_cap_hcw': 0,  # Extra healthcare worker capacity.
        'extra_cap_ct': 0,  # Extra contact tracing/monitoring capacity.
        'extra_ppe': 0,  # Extra personal protective equipment stockpile.
        'extra_delay': 30,  # A one-month delay in intervention delivery.
        'extra_pr_asc': 0,  # Extra boost in case ascertainment.

        # Case fatality rates.
        'cfr_c': 0.70,  # High case fatality rates in the community.
        'cfr_q': 0.70,  # High case fatality rates in home-based isolation.
        'cfr_h': 0.35,  # Evidence for effective hospital treatment.
    }

    if override is not None:
        patch_params.update({k: v for (k, v) in override.items()
                             if k in patch_params})

    exps = np.zeros(patches)
    exps[seed_patch] = 1

    if mixing is None:
        mixing = np.eye(patches)
    elif mixing.shape != (patches, patches):
        raise ValueError("invalid mixing matrix dimensions")
    elif not np.allclose(np.sum(mixing, axis=0), 1):
        raise ValueError("mixing matrix columns do not sum to 1")

    params = {
        'models': 0,
        'patches': patches,
        'patch_names': np.array([str(i).encode('utf-8')
                                 for i in range(patches)]),
        'mixing': mixing,
        'seed': seed,
        'init_exps': exps,
        'time_0': 0,
        'time_1': 7 * 52 * 3,  # Simulate for 3 years.
        'time_dt': 1.0,
        'verbose': False,
        'country': "",
        'intervention': "none",
        'preemptive': False,
        'ct_per_case': 20,  # Contacts identified per case ascertained.
        'exps_per_case': 1,  # Exposed invididuals identified per case.
        'ct_duration': 20,  # The monitoring duration for a traced contact.
        'nb_disp': 0,  # The negative binomial dispersion (0: use Poisson).
    }

    if override is not None:
        params.update({k: v for (k, v) in override.items() if k in params})
        # Warn if any overrides failed to correspond to a valid parameter.
        for k in override:
            if k not in params and k not in patch_params:
                print("Unknown override {}".format(k))

    # Store patch parameters as NumPy arrays.
    for k, v in patch_params.items():
        if isinstance(v, int):
            params[k] = np.zeros(patches, dtype=np.int32)
            params[k].fill(v)
        elif isinstance(v, float):
            params[k] = np.zeros(patches)
            params[k].fill(v)
        elif isinstance(v, list):
            vlen = len(v)
            if vlen == 1:
                params[k] = np.zeros(patches)
                params[k].fill(v[0])
            elif vlen == patches:
                params[k] = np.array(v)
            else:
                raise ValueError("parameter {} has length {}".format(k, vlen))
        elif isinstance(v, np.ndarray):
            vshape = v.shape
            if vshape == (1,):
                params[k] = np.zeros(patches)
                params[k].fill(v[0])
            elif vshape == (patches,):
                params[k] = v
            else:
                raise ValueError("parameter {} has length {}".format(k, vlen))
        else:
            raise ValueError("parameter {} is invalid: {}".format(k, v))

    # Calculate parameter values that are defined by other parameters.
    params['gamma1_c'] = 1 / (1 / params['gamma'] - 1 / params['gamma0_c'])
    params['gamma1_h'] = 1 / (1 / params['gamma'] - 1 / params['gamma0_h'])

    return params


def single_patch_params(params, patch_num):
    """Extract parameters for a single patch."""
    patches = params['patches']
    if patch_num < 0 or patch_num >= patches:
        raise ValueError("Invalid patch number {}".format(patch_num))

    sp = {}
    for k, v in params.items():
        if k == 'patches':
            sp[k] = 1
        elif k == 'mixing':
            sp[k] = np.eye(1)
        elif k == 'init_exps':
            sp[k] = 1
        elif isinstance(v, np.ndarray) and v.shape == (patches,):
            sp[k] = np.array([v[patch_num]])
        elif isinstance(v, (int, float, str)):
            sp[k] = v
        else:
            raise ValueError("Invalid value {} for {}".format(v, k))
    return sp


class C(object):
    """State-vector indices for compartments and counters."""
    Sc = 0
    Sh = 1
    St = 2
    Ec = 3
    Eh = 4
    Et = 5
    I0c = 6
    I0h = 7
    I0t = 8
    Ic = 9
    Ih = 10
    Qc = 11
    Qh = 12
    Hc = 13
    Hh = 14
    Dc = 15
    Dh = 16
    Rc = 17
    Rh = 18
    Bc = 19
    Bh = 20
    PPE = 21
    MaxBed = 22
    MaxCT = 23
    ToFD = 24
    ToInt = 25
    CumI = 26
    CumQ = 27
    CumH = 28
    CumCA = 29
    CumCT = 30
    CumID = 31

    @classmethod
    def count(cls):
        return 32


def init(params):
    """Initialise the RNG and return one or more initial state vectors.

    :param params: The model parameters.
    """

    params['rnd'] = np.random.RandomState(params['seed'])

    Ec = params['init_exps']
    Sh = np.round(params['frac_hcw'] * params['N'])
    Sh = Sh.astype(int)
    Sc = params['N'] - Sh - Ec

    steps = np.ceil((params['time_1'] - params['time_0']) / params['time_dt'])
    steps = steps.astype(int)
    steps += 1
    state = np.zeros((steps, params['models'], C.count(), params['patches']),
                     dtype=np.int32)
    state[0, :, C.Sc, :] = Sc
    state[0, :, C.Sh, :] = Sh
    state[0, :, C.Ec, :] = Ec
    state[0, :, C.PPE, :] = params['ppe_stock']
    state[0, :, C.MaxBed, :] = params['max_bed']
    state[0, :, C.MaxCT, :] = params['max_ct']

    return state


def single_stoch_flow(rnd, dt, rate, lim):
    """Stochastically sample from a deterministic flow rate, subject to a
    finite limit.
    """
    if lim == 0:
        return 0
    det_flow = dt * rate
    flow = rnd.poisson(lam=det_flow)
    while flow > lim:
        flow = rnd.poisson(lam=det_flow)
    return flow


def stoch_flows(rnd, dt, rates, lims):
    """Stochastically sample from deterministic flow rates, subject to finite
    limits.
    """
    if np.any(np.logical_and(rates > 0, lims == 0)):
        errs = np.where(np.logical_and(rates > 0, lims == 0))
        print("ERRS: {}".format(errs))
    if len(rates) == 1:
        return single_stoch_flow(rnd, dt, rates, lims)
    det_flows = dt * rates
    flows = rnd.poisson(lam=det_flows)
    flows[lims == 0] = 0
    mask = np.logical_and(flows > lims, lims > 0)
    while np.any(mask):
        flows[mask] = rnd.poisson(lam=det_flows[mask])
        mask = flows > lims
    return flows


def nbinom_flows(rnd, dt, rates, lims, disp):
    """Stochastically sample from deterministic flow rates, subject to finite
    limits, using negative binomial distributions."""
    if np.any(np.logical_and(rates > 0, lims == 0)):
        errs = np.where(np.logical_and(rates > 0, lims == 0))
        print("ERRS: {}".format(errs))

    # Handle the scalar case separately, since it involves no indexing.
    if len(rates) == 1:
        if lims == 0:
            return 0
        det_flow = dt * rates
        pr = disp / (disp + det_flow)
        flow = rnd.negative_binomial(disp, pr)
        while flow > lims:
            flow = rnd.negative_binomial(disp, pr)
        return flow
    else:
        det_flows = dt * rates
        prs = disp / (disp + det_flows)
        flows = rnd.negative_binomial(disp, prs)
        flows[lims == 0] = 0
        mask = np.logical_and(flows > lims, lims > 0)
        while np.any(mask):
            flows[mask] = rnd.negative_binomial(disp, prs[mask])
            mask = flows > lims
        return flows.astype(np.int32)


def multinom_flows(rnd, flows, probs):
    """Randomly select a destination for each individual.

    :param rnd: The PRNG instance.
    :param flows: The matrix of net outflows.
    :param probs: The probability of each destination.
    """
    out_shape = flows.shape + (len(probs),)
    out = np.zeros(out_shape)
    nz_mask = flows > 0
    if np.any(nz_mask):
        nonz_out = out[nz_mask]
        # Note: the NumPy multinomial distribution is not vectorised with
        #       respect to the number of samples (i.e, the net outflows),
        #       therefore the following loop is not avoidable.
        it = np.nditer(flows[nz_mask], flags=['multi_index'])
        while not it.finished:
            nonz_out[it.multi_index] = rnd.multinomial(it[0], probs)
            it.iternext()
        # Note: this seems to be necessary ...
        out[nz_mask] = nonz_out
    return out


def binom_flows(rnd, net_flow, pr):
    """Calculate probabilistic flows with binomial sampling.
    """
    # Note: net_flow has shape N_SV x N_P.
    #       pr can have shape N_P or N_SV x N_P.
    pos_flows = rnd.binomial(net_flow, pr)
    neg_flows = net_flow - pos_flows
    return (pos_flows, neg_flows)


def step(params, state, t, cur):
    """Simulate a time-step for any number of state vectors.

    :param params: The simulation and model parameters.
    :param state: The matrix of state vectors.
    :param t: The time at the end of the time-step.
    :param cur: The index of the current state vectors.
    """
    nxt = cur + 1

    cur_state = state[cur]
    mix_mat = params['mixing']

    # Identify the model instances where the first case has been detected.
    fd_mask = np.amax(cur_state[:, C.ToFD, :], axis=-1) > 0

    # Increase the available capacities as appropriate for the intervention.
    intv_mask = np.amax(cur_state[:, C.ToInt, :], axis=-1) == nxt
    cur_state[intv_mask, C.PPE, :] += params['extra_ppe']
    cur_state[intv_mask, C.MaxBed, :] += params['extra_cap_bed']
    cur_state[intv_mask, C.Sh, :] += params['extra_cap_hcw']
    cur_state[intv_mask, C.MaxCT, :] += params['extra_cap_ct']

    def s(ix, susc_frac=True):
        """Return the size of a compartment.

        :param ix: The index of the compartment.
        :param frac: Whether to return the fractional size.
        """
        if susc_frac and ix in [C.Sc, C.Sh, C.St]:
            # return state[cur, :, ix, :] / params['N']
            return cur_state[:, ix, :] / params['N']
        else:
            # return state[cur, :, ix, :]
            return cur_state[:, ix, :]

    def p(name, expand=True):
        """Return the vector of values for a given parameters.

        :param name: The name of the parameter.
        :param expand: Whether to expand the leading dimension by one."""
        if expand:
            return params[name][None, :]
        else:
            return params[name]

    # Identify the model instances where social interventions should apply.
    soc_act = (np.amax(cur_state[:, C.ToFD, :], axis=-1)
               + np.max(params['reduce_delay']))
    soc_mask = np.logical_and(fd_mask, cur >= soc_act)

    # Account for reduced infectivity of dead bodies, where appropriate.
    scale_beta_D = np.ones(s(C.Dc).shape)
    scale_beta_D[soc_mask] = params['reduce_beta_D']

    # Account for reduced infectivity in the community, where appropriate.
    scale_beta_I = np.ones(s(C.Ic).shape)
    scale_beta_I[soc_mask] = params['reduce_beta_I']

    # Reduce the community force of infection for HCWs post-ascertainment.
    scale_beta_Ic_h = np.ones(s(C.Dc).shape)
    if np.any(fd_mask):
        scale_beta_Ic_h[fd_mask] = params['beta_Ic_c'] / params['beta_Ic_h']

    # Net force of infection for the general community.
    force_c = (p('beta_I0_c') * scale_beta_I
               * (s(C.I0c) + s(C.I0h) + s(C.I0t)) +
               p('beta_Ic_c') * scale_beta_I * (s(C.Ic) + s(C.Ih)) +
               p('beta_Q_c') * (s(C.Qc) + s(C.Qh)) +
               p('beta_H_c') * (s(C.Hc) + s(C.Hh)) +
               p('beta_D_c') * scale_beta_D * (s(C.Dc) + s(C.Dh)) +
               p('beta_R_c') * (s(C.Rc) + s(C.Rh)))
    # Net force of infection for healthcare workers.
    force_h = (p('beta_I0_h') * scale_beta_I
               * (s(C.I0c) + s(C.I0h) + s(C.I0t)) +
               p('beta_Ic_h') * scale_beta_I
               * scale_beta_Ic_h * (s(C.Ic) + s(C.Ih)) +
               p('beta_Q_h') * (s(C.Qc) + s(C.Qh)) +
               p('beta_H_h') * (s(C.Hc) + s(C.Hh)) +
               p('beta_D_h') * scale_beta_D * (s(C.Dc) + s(C.Dh)) +
               p('beta_R_h') * (s(C.Rc) + s(C.Rh)))
    # Account for mixing between patches.
    # Note: this does not account for differences between visiting another
    #       patch and being visited by someone from another patch.
    force_c = np.dot(force_c, mix_mat)
    force_h = np.dot(force_h, mix_mat)

    # Deterministic flow rates from S to E.
    Sc_to_Ec = force_c * s(C.Sc) ** p('eta')
    Sh_to_Eh = force_h * s(C.Sh) ** p('eta')
    St_to_Et = force_c * s(C.St) ** p('eta')
    # Deterministic flow rates from E to I0.
    Ec_to_I0c = s(C.Ec) * p('sigma')
    Eh_to_I0h = s(C.Eh) * p('sigma')
    Et_to_I0t = s(C.Et) * p('sigma')
    # Deterministic flow rates from I0 to I, Q and H.
    I0c_out = s(C.I0c) * p('gamma0_c')
    I0h_out = s(C.I0h) * p('gamma0_h')
    I0t_out = s(C.I0t) * p('gamma0_c')
    # Deterministic flow rates from I, Q and H to R, D and B.
    Ic_out = s(C.Ic) * p('gamma1_c')
    Ih_out = s(C.Ih) * p('gamma1_c')
    Qc_out = s(C.Qc) * p('gamma1_c')
    Hc_out = s(C.Hc) * p('gamma1_c')
    Qh_out = s(C.Qh) * p('gamma1_h')
    Hh_out = s(C.Hh) * p('gamma1_h')
    # Deterministic flow rates from D to B.
    Dc_to_Bc = s(C.Dc) * p('tau')
    Dh_to_Bh = s(C.Dh) * p('tau')

    # Calculate the stochastic flows out of each compartment.
    outflow = np.array([
        Sc_to_Ec, Sh_to_Eh, St_to_Et, Ec_to_I0c, Eh_to_I0h, Et_to_I0t,
        I0c_out, I0h_out, I0t_out, Ic_out, Ih_out, Qc_out, Qh_out,
        Hc_out, Hh_out, Dc_to_Bc, Dh_to_Bh])
    max_out = np.array([
        s(C.Sc, False), s(C.Sh, False), s(C.St, False),
        s(C.Ec), s(C.Eh), s(C.Et), s(C.I0c), s(C.I0h), s(C.I0t),
        s(C.Ic), s(C.Ih), s(C.Qc), s(C.Qh), s(C.Hc), s(C.Hh),
        s(C.Dc), s(C.Dh)])
    if params['nb_disp'] > 0:
        # Sample non-infection flows from Poisson distributions.
        outflow[3:, :, :] = stoch_flows(params['rnd'], params['time_dt'],
                                        outflow[3:], max_out[3:])
        # Sample the number of infections from negative binomials.
        outflow[:3, :, :] = nbinom_flows(params['rnd'], params['time_dt'],
                                         outflow[:3, :, :], max_out[:3, :, :],
                                         params['nb_disp'])
        outflow = outflow.astype(np.int64)
    else:
        # Sample all flows from Poisson distributions.
        outflow = stoch_flows(params['rnd'], params['time_dt'],
                              outflow, max_out)

    # Invalidate deterministic flows that are no longer relevant.
    I0c_out = None
    I0h_out = None
    I0t_out = None
    Ic_out = None
    Ih_out = None
    Qc_out = None
    Qh_out = None
    Hc_out = None
    Hh_out = None

    # Probabilities of case ascertainment and case fatalities.
    # Note that the probability of case ascertainment depends on whether the
    # first case-detection has occurred.
    # asc_pr = np.array(p('pr_detect', False))
    asc_pr = np.zeros(outflow[C.I0c].shape)
    asc_pr.fill(max(params['pr_detect']))
    # Note: once the first case is ascertained, we elevate this probability.
    asc_pr[fd_mask] = max(params['pr_detect_int'])
    # Allow for a delayed boost in case ascertainment.
    boost_time = np.amax(cur_state[:, C.ToInt, :], axis=-1)
    boost_mask = np.logical_and(boost_time > 0, boost_time <= nxt)
    asc_pr[boost_mask] += params['extra_pr_asc']
    cfr_c = np.array(p('cfr_c', False))
    cfr_q = np.array(p('cfr_q', False))
    cfr_h = np.array(p('cfr_h', False))

    # Determine the stochastic flows between compartments.
    Sc_to_Ec = outflow[C.Sc]
    Sh_to_Eh = outflow[C.Sh]
    St_to_Et = outflow[C.St]
    Ec_to_I0c = outflow[C.Ec]
    Eh_to_I0h = outflow[C.Eh]
    Et_to_I0t = outflow[C.Et]
    I0c_to_QorH, I0c_to_Ic = binom_flows(params['rnd'], outflow[C.I0c], asc_pr)
    I0h_out = outflow[C.I0h]
    I0t_out = outflow[C.I0t]
    Ic_to_Dc, Ic_to_Rc = binom_flows(params['rnd'], outflow[C.Ic], cfr_c)
    Ih_to_Dh, Ih_to_Rh = binom_flows(params['rnd'], outflow[C.Ih], cfr_c)
    Qc_to_Dc, Qc_to_Rc = binom_flows(params['rnd'], outflow[C.Qc], cfr_q)
    Qh_to_Dh, Qh_to_Rh = binom_flows(params['rnd'], outflow[C.Qh], cfr_q)
    Hc_to_Dc, Hc_to_Rc = binom_flows(params['rnd'], outflow[C.Hc], cfr_h)
    Hh_to_Dh, Hh_to_Rh = binom_flows(params['rnd'], outflow[C.Hh], cfr_h)
    Dc_to_Bc = outflow[C.Dc]
    Dh_to_Bh = outflow[C.Dh]

    # Contact-tracing flows, all initially set to zero.
    Sc_to_St = Sc_to_Ec * 0
    St_to_Sc = Sc_to_Ec * 0
    Ec_to_Et = Sc_to_Ec * 0
    I0c_to_I0t = Sc_to_Ec * 0
    # Contact-tracing counts.
    ct_CA = I0c_to_QorH
    ct_CT = 0 * ct_CA
    ct_ID = 0 * ct_CA
    ct_inflow = 0 * ct_CA
    if np.any(fd_mask):
        num_asc = I0c_to_QorH[fd_mask]
        # Calculate the current contact-tracing load.
        ct_load = (s(C.St, False) + s(C.Et) + s(C.I0t))[fd_mask]
        # Calculate the available contact-tracing capacity (non-negative).
        ct_cap = np.maximum(s(C.MaxCT)[fd_mask] - ct_load, 0)
        # Calculate the number of new contacts, subject to available capacity.
        ct_per_case = params['ct_per_case']
        ct_inflow[fd_mask] = np.minimum(num_asc * ct_per_case, ct_cap)
        ct_CT = ct_inflow
        # Stochastically select the number of exposed individuals that are
        # identified as a result of the contact tracing.
        nonz_ct = ct_inflow > 0
        # Calculate the (stochastic) contact-tracing outflow (St -> Sc).
        St_to_Sc = stoch_flows(params['rnd'], params['time_dt'],
                               s(C.St, False) / params['ct_duration'],
                               s(C.St, False))
        # Continue tracing individuals that have just become exposed.
        St_to_Sc = np.minimum(St_to_Sc, s(C.St, False) - St_to_Et)
        if np.any(nonz_ct):
            # Calculate the (deterministic) contact-tracing inflow (Sc -> St).
            exp_frac = params['exps_per_case'] / ct_per_case
            susc_frac = 1 - exp_frac
            Sc_to_St[nonz_ct] = np.floor(ct_inflow[nonz_ct] * susc_frac)
            # Cap contact-tracing of susceptible individuals not only by the
            # available capacity, but also by the number of such individuals!
            # When sampling secondary cases from a negative binomial with a
            # large amount of dispersion (k < 1), there is a chance that the
            # number of susceptible individuals will be smaller than Sc_to_St.
            max_to_St = s(C.Sc, False)[nonz_ct] - Sc_to_Ec[nonz_ct]
            Sc_to_St[nonz_ct] = np.minimum(Sc_to_St[nonz_ct], max_to_St)
            # Stochastically identify one exposure per traced case.
            exp_to_find = ct_inflow[nonz_ct] * exp_frac
            found = stoch_flows(params['rnd'], params['time_dt'], exp_to_find,
                                s(C.Ec)[nonz_ct] + s(C.I0c)[nonz_ct])
            ct_ID[nonz_ct] = found
            # Pick identified cases from Ec and/or I0c, move to Et and/or I0t.
            in_ec = s(C.Ec)[nonz_ct]
            in_i0c = s(C.I0c)[nonz_ct]
            pr_from_ec = in_ec / (in_ec + in_i0c)
            pick_ec, pick_i0c = binom_flows(params['rnd'], found, pr_from_ec)
            Ec_to_Et[nonz_ct] = pick_ec
            I0c_to_I0t[nonz_ct] = pick_i0c

    # Calculate the limited capacities that remain available.
    hcw_frac = (s(C.Sh, False) + s(C.Rh)) / (p('N') * p('frac_hcw'))
    # Truncate the proportion of available HCWs to the interval [0.8, 1.0].
    hcw_frac = np.maximum(0.8, hcw_frac)
    hcw_frac = np.minimum(1.0, hcw_frac)
    # Use the quantile function of a beta distribution to smoothly decrease
    # the healthcare capacity as a function of the proportion of HCWs that
    # remain available (i.e., either susceptible or recovered).
    #
    #     HCW available    Isolation beds available
    #     -----------------------------------------
    #     100%             100%
    #      90%              50%
    #      80%               0%
    bed_frac = sp.beta.ppf(hcw_frac - 0.4, a=0.01, b=0.01)
    cap_bed = np.round(s(C.MaxBed) * bed_frac - s(C.Hc) - s(C.Hh))
    cap_bed = cap_bed.astype(int)
    cap_bed = np.maximum(0, cap_bed)
    cap_ppe = s(C.PPE)

    # Home-based isolation cases consume the PPE stockpile.
    home_reqs_ppe = True

    # Flows from I0h to Hh and Qh, with overflow into Ih.
    # Note that we account for HCW flow before general cases, under the
    # assumption that hospital beds are preferentially allocated to HCWs.
    if np.any(p('pr_detect') > 0):
        I0h_to_Hh = np.minimum(cap_bed, I0h_out)
        I0h_rem = I0h_out - I0h_to_Hh
        # Account for the reduction in capacity.
        cap_bed -= I0h_to_Hh
        # Overflow into home-based isolation.
        if home_reqs_ppe:
            I0h_to_Qh = np.minimum(I0h_rem, cap_ppe)
            cap_ppe -= I0h_to_Qh
        else:
            I0h_to_Qh = I0h_rem
        # Overflow into the general community.
        I0h_to_Ih = I0h_rem - I0h_to_Qh
        I0h_rem = None
        I0h_out = None
    else:
        I0h_to_Ih = I0h_out
        I0h_to_Hh = np.zeros(params['patches'])
        I0h_to_Qh = np.zeros(params['patches'])
        I0h_out = None

    # Flows from I0t to Hc and Qc, with overflow into Ic.
    I0t_to_Hc = np.minimum(cap_bed, I0t_out)
    # Account for the reduction in capacity.
    cap_bed -= I0t_to_Hc
    I0t_rem = I0t_out - I0t_to_Hc
    # Overflow into home-based isolation.
    if home_reqs_ppe:
        I0t_to_Qc = np.minimum(cap_ppe, I0t_rem)
        cap_ppe -= I0t_to_Qc
    else:
        I0t_to_Qc = I0t_rem
    # Overflow into the general community.
    I0t_to_Ic = I0t_rem - I0t_to_Qc
    I0t_rem = None
    I0t_out = None

    # Flows from I0c to Hc and Qc, with overflow into Ic.
    I0c_to_Hc = np.minimum(cap_bed, I0c_to_QorH)
    # Account for the reduction in capacity.
    cap_bed -= I0c_to_Hc
    I0c_rem = I0c_to_QorH - I0c_to_Hc
    # Overflow into home-based isolation.
    if home_reqs_ppe:
        I0c_to_Qc = np.minimum(cap_ppe, I0c_rem)
        cap_ppe -= I0c_to_Qc
    else:
        I0c_to_Qc = I0c_rem
    # Overflow into the general community.
    I0c_to_Ic += (I0c_rem - I0c_to_Qc)
    I0c_rem = None
    I0c_to_QorH = None

    # Calculate the net outflow for the I0c and I0t compartments.
    # Note: contact-tracing flows (I0c -> I0t) are ignored here.
    I0c_out = I0c_to_Ic + I0c_to_Hc + I0c_to_Qc
    I0t_out = I0t_to_Ic + I0t_to_Hc + I0t_to_Qc

    # Calculate the net inflows for each compartment.
    flows = np.zeros(state[nxt, ...].shape)
    flows[:, C.Sc, :] = St_to_Sc - Sc_to_Ec - Sc_to_St
    flows[:, C.Sh, :] = - Sh_to_Eh
    flows[:, C.St, :] = Sc_to_St - St_to_Et - St_to_Sc
    flows[:, C.Ec, :] = Sc_to_Ec - Ec_to_I0c
    flows[:, C.Eh, :] = Sh_to_Eh - Eh_to_I0h
    flows[:, C.Et, :] = St_to_Et - Et_to_I0t
    flows[:, C.I0c, :] = Ec_to_I0c - I0c_out
    flows[:, C.I0h, :] = Eh_to_I0h - I0h_to_Ih - I0h_to_Hh - I0h_to_Qh
    flows[:, C.I0t, :] = Et_to_I0t - I0t_out
    flows[:, C.Ic, :] = I0c_to_Ic + I0t_to_Ic - Ic_to_Dc - Ic_to_Rc
    flows[:, C.Ih, :] = I0h_to_Ih - Ih_to_Dh - Ih_to_Rh
    flows[:, C.Qc, :] = I0c_to_Qc + I0t_to_Qc - Qc_to_Dc - Qc_to_Rc
    flows[:, C.Qh, :] = I0h_to_Qh - Qh_to_Dh - Qh_to_Rh
    flows[:, C.Hc, :] = I0c_to_Hc + I0t_to_Hc - Hc_to_Dc - Hc_to_Rc
    flows[:, C.Hh, :] = I0h_to_Hh - Hh_to_Dh - Hh_to_Rh
    flows[:, C.Dc, :] = Ic_to_Dc + Qc_to_Dc + Hc_to_Dc - Dc_to_Bc
    flows[:, C.Dh, :] = Ih_to_Dh + Qh_to_Dh + Hh_to_Dh - Dh_to_Bh
    flows[:, C.Rc, :] = Ic_to_Rc + Qc_to_Rc + Hc_to_Rc
    flows[:, C.Rh, :] = Ih_to_Rh + Qh_to_Rh + Hh_to_Rh
    flows[:, C.Bc, :] = Dc_to_Bc
    flows[:, C.Bh, :] = Dh_to_Bh

    # Validation: ensure all flows are integers.
    nonint_cx = np.where(np.floor(flows) != flows)[1]
    if len(nonint_cx) > 0:
        raise ValueError("Non-integer flows at {}".format(nonint_cx))

    # Validation: ensure the flows sum to zero.
    flow_sum = np.sum(flows)
    if flow_sum != 0:
        raise ValueError("Non-zero net flow of {}".format(flow_sum))

    # Record the updated state vectors.
    state[nxt, ...] = cur_state + flows

    # Now account for the contact-tracing flows Ec -> Et and I0c -> I0t.
    lim_Ec_to_Et = np.minimum(Ec_to_Et, state[nxt, :, C.Ec])
    ext_Ec_to_Et = Ec_to_Et - lim_Ec_to_Et
    lim_I0c_to_I0t = np.minimum(I0c_to_I0t + ext_Ec_to_Et,
                                state[nxt, :, C.I0c])
    state[nxt, :, C.Ec, :] -= lim_Ec_to_Et
    state[nxt, :, C.Et, :] += lim_Ec_to_Et
    state[nxt, :, C.I0c, :] -= lim_I0c_to_I0t
    state[nxt, :, C.I0t, :] += lim_I0c_to_I0t
    ct_ID = lim_Ec_to_Et + lim_I0c_to_I0t

    if np.any(ct_ID[ct_CT == 0] > 0):
        print("Non-zero ID for zero CT")

    if np.any(state[nxt, ...] < 0):
        raise ValueError("Negative compartment")

    # Update the cumulative counters and stockpiles.
    net_to_H = I0c_to_Hc + I0t_to_Hc + I0h_to_Hh
    net_to_Q = I0c_to_Qc + I0t_to_Qc + I0h_to_Qh
    net_to_I = I0c_to_Ic + I0t_to_Ic + I0h_to_Ih + net_to_H + net_to_Q
    state[nxt, :, C.PPE, :] = cap_ppe
    state[nxt, :, C.CumI, :] = cur_state[..., C.CumI, :] + net_to_I
    state[nxt, :, C.CumQ, :] = cur_state[..., C.CumQ, :] + net_to_Q
    state[nxt, :, C.CumH, :] = cur_state[..., C.CumH, :] + net_to_H
    state[nxt, :, C.CumCA, :] = cur_state[:, C.CumCA, :] + ct_CA
    state[nxt, :, C.CumCT, :] = cur_state[:, C.CumCT, :] + ct_CT
    state[nxt, :, C.CumID, :] = cur_state[:, C.CumID, :] + ct_ID

    # Identify the models where the nth case has been detected, and schedule
    # the delivery of the subsequent intervention (if any).
    fd_mask = np.amax(state[nxt, :, C.ToFD, :], axis=-1) == 0
    nth = max(params['detect_nth'])
    delay = int(np.ceil(max(params['extra_delay'] * params['time_dt'])))
    cc_mask = np.sum(state[nxt, :, C.CumI, :], axis=-1) >= nth
    fd_mask = np.logical_and(fd_mask, cc_mask)
    state[nxt, fd_mask, C.ToFD, :] = nxt
    state[nxt, fd_mask, C.ToInt, :] = nxt + delay


def test(vecs=1, max_steps=10):
    mix_mat = np.array([[0.9, 0.1, 0.2],
                        [0.0, 0.8, 0.2],
                        [0.1, 0.1, 0.6]])
    p = params(12e5, patches=3, mixing=mix_mat)
    sv = init(p, vecs=vecs)
    # Restrict the number of steps.
    steps = min(int(max_steps), sv.shape[0] - 1)
    for i in range(steps):
        step(p, sv, 1, i)
    return(p, sv)
