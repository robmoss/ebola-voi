"""A suite of test cases for the infection model."""

import numpy as np

import pybola.model
from pybola.model import C


class FakePRNG(object):
    """A fake pseudo-random number generator that implements only the minimal
    number of functions required by pybola.model.
    """

    def __init__(self, round_up=True):
        """Determine whether flow rates are rounded up (default) or down."""
        self.__round_up = round_up

    def poisson(self, lam):
        """Round expected flow rates either up or down."""
        if self.__round_up:
            return np.ceil(lam).astype(int)
        else:
            return np.floor(lam).astype(int)

    def binomial(self, net_flow, pr):
        """Round expected successes to the nearest integer."""
        sampled_flow = np.round(net_flow * pr).astype(int)
        return sampled_flow

    def negative_binomial(self, net_flow, pr):
        return self.binomial(net_flow, pr)


class TestModel(object):
    """Test cases for the infection model."""

    # The number of model instances to simulate in parallel.
    __num_models = 10

    def setup(self):
        """Define common test parameters.
        """
        self.base_params = pybola.model.params(1e6)
        self.base_params['models'] = self.__num_models
        self.base_params['time_dt'] = 1.0

    def test_init(self):
        """Verify that the initial state vector has correct dimensions.
        """
        sv = pybola.model.init(self.base_params)
        assert sv[0].shape == (self.__num_models, pybola.model.C.count(), 1)

    def test_no_infs(self):
        """Verify that no infections occur without an infectious individual.
        """
        params = self.base_params.copy()
        params['time_1'] = 42  # Simulate for 6 weeks.
        sv = pybola.model.init(params)
        sv[0, :, C.Ec] = 0  # Undo the initial exposure.

        # Run the simulation.
        t = params['time_0']
        for i in range(t, params['time_1']):
            t += 1
            pybola.model.step(params, sv, t, i)

        # Check that the susceptible populations have not changed.
        assert np.all(sv[0, :, C.Sc] == sv[-1, :, C.Sc])
        assert np.all(sv[0, :, C.Sh] == sv[-1, :, C.Sh])
        assert np.all(sv[0, :, C.St] == sv[-1, :, C.St])

    def test_infs(self):
        """Verify that infections occur if there is an infectious individual,
        in the absence of recovery or death.
        """
        params = self.base_params.copy()
        params['time_1'] = 70  # Simulate for 10 weeks.
        params['gamma1_c'][:] = 0  # Prevent recovery/death.
        params['gamma1_h'][:] = 0  # Prevent recovery/death.
        sv = pybola.model.init(params)

        # Run the simulation.
        t = params['time_0']
        for i in range(t, params['time_1']):
            t += 1
            pybola.model.step(params, sv, t, i)

        # Check that no susceptible sub-population increased (they may,
        # however, have remained unchanged).
        assert np.all(sv[0, :, C.Sc] >= sv[-1, :, C.Sc])
        assert np.all(sv[0, :, C.Sh] >= sv[-1, :, C.Sh])
        assert np.all(sv[0, :, C.St] >= sv[-1, :, C.St])
        # Check that the susceptible population decreased in each model.
        s_ixs = np.array([C.Sc, C.Sh, C.St])
        susc_0 = np.sum(sv[0, :, s_ixs], axis=0)
        susc_1 = np.sum(sv[-1, :, s_ixs], axis=0)
        assert np.all(susc_0 > susc_1)

    def test_contact_tracing_occurs(self):
        """Verify that contacts are traced when cases are ascertained.
        """
        params = self.base_params.copy()
        params['time_1'] = 70  # Simulate for 10 weeks.
        params['gamma1_c'][:] = 0  # Prevent recovery/death.
        params['gamma1_h'][:] = 0  # Prevent recovery/death.
        params['detect_nth'][:] = 1  # Detect the 1st case.
        params['pr_detect'][:] = 1.0  # Perfect case ascertainment.
        params['pr_detect_int'][:] = 1.0  # Perfect case ascertainment.
        params['max_ct'][:] = 100  # Enable contact tracing.
        sv = pybola.model.init(params)

        # Run the simulation.
        t = params['time_0']
        for i in range(t, params['time_1']):
            t += 1
            pybola.model.step(params, sv, t, i)

        # Check that some contacts have been traced.
        ct_ixs = np.array([C.St, C.Et, C.I0t])
        # Verify that there was no contact tracing at the start.
        assert np.all(np.sum(sv[0, :, ct_ixs], axis=0) == 0)
        # Verify that contacts are being traced at the end.
        assert np.all(np.sum(sv[-1, :, ct_ixs], axis=0) > 0)
        # Verify that some of these contacts have been exposed.
        assert np.all(np.sum(sv[-1, :, [C.Et, C.I0t]], axis=0) > 0)

    def test_contact_tracing_flows_1(self):
        """Verify that contact-tracing flows are as expected, when the
        available capacity is not exceeded.
        """
        params = self.base_params.copy()
        params['time_1'] = 1  # Simulate for 1 day.
        params['gamma1_c'][:] = 0  # Prevent recovery/death.
        params['gamma1_h'][:] = 0  # Prevent recovery/death.
        params['detect_nth'][:] = 1  # Detect the 1st case.
        params['pr_detect'][:] = 1.0  # Perfect case ascertainment.
        params['pr_detect_int'][:] = 1.0  # Perfect case ascertainment.
        params['max_ct'][:] = 200  # Enable contact tracing.
        sv = pybola.model.init(params)
        sv[0, :, C.Ec] = 0  # Undo the initial exposure.
        # Plant infectious individuals, some of which will be identified
        # during the first time-step and lead to contact-tracing occurring.
        sv[0, :, C.I0c] = 20
        # Enable contact tracing in the first time-step.
        sv[0, :, C.ToFD] = 1
        # Use a deterministic PRNG so that we can evaluate flows.
        params['rnd'] = FakePRNG(round_up=False)

        # Run the simulation.
        t = params['time_0']
        for i in range(t, params['time_1']):
            t += 1
            pybola.model.step(params, sv, t, i)

        # Since gamma_0 = 1/4, we expect 5 individuals from I0c will move to
        # Ic and be ascertained, leading to 100 contacts being traced.
        # Of these 100 contacts, 95 should be susceptible individuals and 5
        # should already be exposed (i.e., taken from Ec or I0c).
        # Since Ec is empty, all 5 should be taken from I0c.
        # In addition, the 20 individuals in I0c should infect slightly less
        # than 2 susceptibles (since S/N is just less than 1).
        assert np.all(sv[-1, :, C.St] == 95)
        assert np.all(sv[-1, :, C.Et] == 0)
        assert np.all(sv[-1, :, C.Ec] == 1)
        assert np.all(sv[-1, :, C.I0t] == 5)
        assert np.all(sv[-1, :, C.I0c] == 10)
        assert np.all(sv[-1, :, C.Ic] == 5)

    def test_contact_tracing_flows_2(self):
        """Verify that contact-tracing flows are as expected, when the
        available capacity is exceeded.
        """
        params = self.base_params.copy()
        params['time_1'] = 1  # Simulate for 1 day.
        params['gamma1_c'][:] = 0  # Prevent recovery/death.
        params['gamma1_h'][:] = 0  # Prevent recovery/death.
        params['detect_nth'][:] = 1  # Detect the 1st case.
        params['pr_detect'][:] = 1.0  # Perfect case ascertainment.
        params['pr_detect_int'][:] = 1.0  # Perfect case ascertainment.
        params['max_ct'][:] = 60  # Enable contact tracing.
        sv = pybola.model.init(params)
        sv[0, :, C.Ec] = 0  # Undo the initial exposure.
        # Plant infectious individuals, some of which will be identified
        # during the first time-step and lead to contact-tracing occurring.
        sv[0, :, C.I0c] = 20
        # Enable contact tracing in the first time-step.
        sv[0, :, C.ToFD] = 1
        # Use a deterministic PRNG so that we can evaluate flows.
        params['rnd'] = FakePRNG(round_up=False)

        # Run the simulation.
        t = params['time_0']
        for i in range(t, params['time_1']):
            t += 1
            pybola.model.step(params, sv, t, i)

        # Since gamma_0 = 1/4, we expect 5 individuals from I0c will move to
        # Ic and be ascertained, leading to 100 contacts being traced; but
        # there is only capacity to trace 60.
        # Of these 60 contacts, 57 should be susceptible individuals and 3
        # should already be exposed (i.e., taken from Ec or I0c).
        # Since Ec is empty, all 3 should be taken from I0c.
        # In addition, the 20 individuals in I0c should infect slightly less
        # than 2 susceptibles (since S/N is just less than 1).
        assert np.all(sv[-1, :, C.St] == 57)
        assert np.all(sv[-1, :, C.Et] == 0)
        assert np.all(sv[-1, :, C.Ec] == 1)
        assert np.all(sv[-1, :, C.I0t] == 3)
        assert np.all(sv[-1, :, C.I0c] == 12)
        assert np.all(sv[-1, :, C.Ic] == 5)
