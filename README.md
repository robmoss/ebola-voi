# Stochastic meta-population model of Ebola transmission

This directory contains a meta-population (multi-patch) stratified compartment
model of Ebola transmission in a human population, which distinguishes
health-care workers (HCWs) from the general population, and which accounts for
the effects of medical and non-medical interventions (subject to finite
capacities and resource stockpiles).

## License

This work is distributed under the terms of the BSD 3-Clause License, as
documented in the file `LICENSE`.

## Usage

To simulate model scenarios, see the output of:

    ./bin/scenario.py --help

To plot simulation outputs, see the output of:

    ./bin/plot.R --help

## Installation

Create a virtual environment and install the required packages:

    EBOLA_VENV="./venv"
    virtualenv -p python3 "${EBOLA_VENV}"
    . "${EBOLA_VENV}"/bin/activate
    pip install -r requirements.txt

## Test cases

Run the test cases:

    python setup.py test
