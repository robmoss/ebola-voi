#!/usr/bin/env python
"""
This script tests the effects of delaying an improvement to surveillance or
the local health care system.
"""

import argparse
import numpy as np
import os
import sys


def arg_parser():
    p = argparse.ArgumentParser(
        usage='%(prog)s [options | --help]',
        formatter_class=argparse.RawTextHelpFormatter,
        add_help=False)

    sg = p.add_argument_group('Scenario arguments')
    sg.add_argument('--country', metavar='COUNTRY', default=None, type=str,
                    choices=['png'],
                    help='The country for the model scenarios.\n'
                    'Values: png.')

    bg = p.add_argument_group('Boost case ascertainment')
    bg.add_argument('--boost-min', default=0.2, type=float, metavar='PR',
                    help='The minimum case ascertainment boost'
                    ' (default is %(default)s).')
    bg.add_argument('--boost-max', default=0.6, type=float, metavar='PR',
                    help='The maximum case ascertainment boost'
                    ' (default is %(default)s).')
    bg.add_argument('--boost-step', default=0.2, type=float, metavar='PR',
                    help='The boost increment (default is %(default)s).')
    bg.add_argument('--delay-min', default=0, type=int, metavar='WEEKS',
                    help='The minimum delay (default is %(default)s).')
    bg.add_argument('--delay-max', default=6, type=int, metavar='WEEKS',
                    help='The maximum delay (default is %(default)s).')
    bg.add_argument('--delay-step', default=1, type=int, metavar='WEEKS',
                    help='The increment in delay (default is %(default)s).')

    pg = p.add_argument_group('Simulation arguments')
    pg.add_argument('--models', type=int, metavar='N', default=500,
                    help='The number of models (default is %(default)s).\n ')
    pg.add_argument('--seed', type=int, default=42,
                    help='The PRNG seed (default is %(default)s).\n ')

    mg = p.add_argument_group('Miscellaneous arguments')
    mg.add_argument('-h', '--help', action='help',
                    help='Show this usage information.\n ')
    mg.add_argument('--verbose', action='store_true',
                    help='Display logging messages.\n ')

    return p


def _run(params, boost_asc, delay):
    import pybola.sim

    # Simulate the scenario and collect the results.
    sv_table = pybola.sim.run(params)

    # Calculate the epidemic outcomes.
    epi_die, epi_sml, epi_lrg = pybola.sim.outcomes(params, sv_table)

    # Print the epidemic outcomes.
    ctry = params['country']
    base_pr = np.max(params['pr_detect_int'])
    boosted_pr = base_pr + boost_asc
    print("{},{},{},{},{:0.3f},{:0.3f},{:0.3f}".format(
        ctry, base_pr, boosted_pr, delay, epi_die, epi_sml, epi_lrg))

    # Ensure the output appears in a timely manner when using "--all".
    sys.stdout.flush()


def _params(seed, ctry, boost_asc, delay, models):
    import pybola.country
    import pybola.inter

    # Determine the appropriate parameters for the scenario.
    scenario = pybola.inter.overrides('none', False)
    if ctry == 'png':
        params = pybola.country.png(override=scenario, seed=seed)
    else:
        raise ValueError("Invalid country {}".format(ctry))

    # Set the number of models to run in parallel.
    params['models'] = models

    # Set the delay.
    params['extra_delay'][:] = 7 * delay
    # params['extra_pr_asc'] = np.zeros(params['extra_delay'].shape)
    params['extra_pr_asc'][:] = boost_asc

    return params


def main(args=None):

    p = arg_parser()
    args = p.parse_args(args)

    print(",".join(["Country", "BaselineAsc", "BoostedAsc", "Delay",
                    "PrDieOut", "PrSmall", "PrLarge"]))
    countries = ['png']
    if args.country is not None:
        countries = [args.country.lower()]

    bmin = args.boost_min
    bstep = args.boost_step
    bmax = args.boost_max + 0.5 * bstep

    for ctry in countries:
        for boost in np.arange(bmin, bmax, bstep):
            for delay in range(args.delay_min, args.delay_max + 1):
                params = _params(args.seed, ctry, boost, delay, args.models)
                _run(params, boost, delay)


if __name__ == '__main__':
    # Ensure that the parent directory, which contains the pybola package, is
    # included in the module search path.
    script_dir = os.path.dirname(__file__)
    parent_dir = os.path.abspath(os.path.join(script_dir, '..'))
    has_pybola = os.path.isdir(os.path.join(parent_dir, "pybola"))
    if has_pybola and parent_dir not in sys.path:
        sys.path.insert(1, parent_dir)

    sys.exit(main())
