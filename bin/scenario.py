#!/usr/bin/env python

import argparse
import numpy as np
import os
import sys


def arg_parser():
    p = argparse.ArgumentParser(
        usage='%(prog)s [options | --help]',
        formatter_class=argparse.RawTextHelpFormatter,
        add_help=False)

    sg = p.add_argument_group('Scenario arguments')
    sg.add_argument('--all', action='store_true', default=False,
                    help='Simulate all scenarios.\n ')
    sg.add_argument('--single-patch', action='store_true', default=False,
                    help='Simulate each patch in a country separately.\n ')
    sg.add_argument('--only-patch', metavar='NTH', default=None, type=int,
                    help='Simulate the NTH patch in a country.\n ')
    sg.add_argument('--country', metavar='COUNTRY', default=None, type=str,
                    choices=['png'],
                    help='The country for the model scenarios.\n'
                    'Values: png.\n ')
    sg.add_argument('--preemptive', action='store_true',
                    help='Preemptively improve surveillance.\n ')
    sg.add_argument('--intervention', metavar='INT', default='none', type=str,
                    choices=['none', 'quick', 'slow'],
                    help='The intervention for the model scenarios.\n'
                    'Values: none (default), quick, slow.\n ')
    sg.add_argument('--disp', metavar='K', default=0, type=float,
                    help='The negative binomial dispersion for infections.\n'
                    'The default (0) is to sample from the Poisson.')
    sg.add_argument('--baseline', action='store_true',
                    help='Only explore the baseline scenario in each patch.')
    sg.add_argument('--reduced-beta', action='store_true',
                    help='Only explore the reduced infection scenario.')

    mg = p.add_argument_group('Multiple scenarios')
    mg.add_argument('--pr-min', default=0.1, type=float,
                    help='The minimum case ascertainment probability')
    mg.add_argument('--pr-max', default=0.9, type=float,
                    help='The maximum case ascertainment probability')
    mg.add_argument('--pr-step', default=0.1, type=float,
                    help='The increment in case ascertainment probability')

    pg = p.add_argument_group('Simulation arguments')
    pg.add_argument('--models', type=int, metavar='N', default=500,
                    help='The number of models (default is 500).\n ')
    pg.add_argument('--seed', type=int, default=42,
                    help='The PRNG seed (default is 42).\n ')
    pg.add_argument('--output', type=str, metavar='FILE', required=False,
                    help='The output file.\n ')
    pg.add_argument('--no-output', action='store_true',
                    help='Do not save time-series data.\n ')

    cg = p.add_argument_group('Concurrency arguments')
    cg.add_argument('--parts', type=int, metavar='P', default=1,
                    help='Partition into P sets (default is 1).\n ')
    cg.add_argument('--nth', type=int, metavar='NTH', default=1,
                    help='Run the NTH partition (default is 1).\n ')

    mg = p.add_argument_group('Miscellaneous arguments')
    mg.add_argument('-h', '--help', action='help',
                    help='Show this usage information.\n ')
    mg.add_argument('--verbose', action='store_true',
                    help='Display logging messages.\n ')

    return p


def _run(params, preemptive, interv, output):
    import pybola.sim

    # Simulate the scenario and collect the results.
    sv_table = pybola.sim.run(params)

    # Calculate the epidemic outcomes.
    epi_die, epi_sml, epi_lrg = pybola.sim.outcomes(params, sv_table)

    # Print the epidemic outcomes.
    ctry = params['country']
    pre_lbl = "Yes" if preemptive else "No"

    if params['patches'] == 1:
        patch = params['patch_names'][0]
        beta_C = params['beta_Ic_c'][0]
        red_bI = params['reduce_beta_I'][0]
        red_bD = params['reduce_beta_D'][0]
        int_lbl = {"none": "None", "quick": "Small", "slow": "Large"}[interv]
        fd_nth = int(params['detect_nth'][0])
        pr_asc = params['pr_detect_int'][0]
        disp = params['nb_disp']
        print("{},{},{},{},{},{},{},{},{},{:0.3f},{:0.3f},{:0.3f}".format(
            ctry, patch, pr_asc, int_lbl, fd_nth, beta_C, red_bI, red_bD,
            disp, epi_die, epi_sml, epi_lrg))
    else:
        int_lbl = {"none": "No", "quick": "Small", "slow": "Large"}[interv]
        pr_asc = params['pr_detect_int'][0]
        disp = params['nb_disp']
        print("{},{},{},{},{},{:0.3f},{:0.3f},{:0.3f}".format(
            ctry, pre_lbl, int_lbl, pr_asc, disp, epi_die, epi_sml, epi_lrg))

    # Ensure the output appears in a timely manner when using "--all".
    sys.stdout.flush()

    # Save the results to disk.
    if output is not False:
        pybola.sim.save_table(output, params, sv_table)


def _params(seed, ctry, preemptive, interv, models, pr_asc=None, k=0):
    import pybola.country
    import pybola.inter

    # Determine the appropriate parameters for the scenario.
    scenario = pybola.inter.overrides(interv, preemptive)
    if ctry == 'png':
        params = pybola.country.png(override=scenario, seed=seed)
    else:
        raise ValueError("Invalid country {}".format(ctry))

    # Set the number of models to run in parallel.
    params['models'] = models

    if pr_asc is not None:
        params['pr_detect_int'] = pr_asc * np.ones(params['patches'])

    params['nb_disp'] = k

    return params


def run_scenario(seed, ctry, preemptive, interv, models, k, pr_asc, output):
    params = _params(seed, ctry, preemptive, interv, models, pr_asc, k)
    if output is None:
        pre = "preempt" if preemptive else "react"
        pr_asc = params['pr_detect_int'][0]
        out_fmt = "{}-{}-{}-{}-{}.hdf5"
        output = out_fmt.format(ctry, pre, interv, models, pr_asc)

    _run(params, preemptive, interv, output)


def main(args=None):

    p = arg_parser()
    args = p.parse_args(args)
    interv = args.intervention.lower()
    k = args.disp

    if args.all:
        pmin = args.pr_min
        pstep = args.pr_step
        pmax = args.pr_max + 0.5 * pstep
        output = False if args.no_output else None
        print(",".join(["Country", "Preemptive", "Reactive", "PrCaseAsc",
                        "Disp", "PrDieOut", "PrSmall", "PrLarge"]))
        countries = ['png']
        if args.country is not None:
            countries = [args.country.lower()]
        for ctry in countries:
            for pr_asc in np.arange(pmin, pmax, pstep):
                for interv in ['none', 'quick', 'slow']:
                    for pre in [True, False]:
                        run_scenario(args.seed, ctry, pre, interv,
                                     args.models, k, pr_asc, output)
    elif not args.country:
        raise ValueError("Must specify --all or --country")

    if args.single_patch:
        import pybola.model
        pmin = args.pr_min
        pstep = args.pr_step
        pmax = args.pr_max + 0.5 * pstep
        country = args.country.lower()
        output = False if args.no_output else args.output
        pre = False

        counter = 0
        if args.nth < 1 or args.nth > args.parts:
            print("ERROR: invalid value for --nth")
            return

        beta_C_vals = [0.1, 0.2, 0.3]
        red_bD_vals = [1.0, 0.75, 0.5]
        red_bI_vals = [1.0, 0.75, 0.5]
        interv_vals = ['none', 'quick', 'slow']
        nth_vals = [5, 10, 25, 50]

        if args.baseline:
            # Don't explore multiple values for these parameters.
            beta_C_vals = [0.2]
            red_bD_vals = [1.0]
            red_bI_vals = [1.0]
            interv_vals = ['none']
            nth_vals = [10]
        elif args.reduced_beta:
            # Don't explore multiple values for these parameters.
            beta_C_vals = [0.2]
            red_bD_vals = [0.5]
            red_bI_vals = [0.5]
            interv_vals = ['none']
            nth_vals = [10]

        print(",".join(["Country", "Region", "PrCaseAsc", "Intervention",
                        "FD_nth", "BetaC", "ScaleBetaI", "ScaleBetaD",
                        "Disp", "PrDieOut", "PrSmall", "PrLarge"]))
        for pr_asc in np.arange(pmin, pmax, pstep):
            for beta_C in beta_C_vals:
                for red_bD in red_bD_vals:
                    for red_bI in red_bI_vals:
                        for interv in interv_vals:
                            params = _params(args.seed, country, pre,
                                             interv, args.models, pr_asc, k)
                            for nth in nth_vals:
                                params['detect_nth'][:] = nth
                                if args.only_patch is not None:
                                    patches = [args.only_patch]
                                else:
                                    patches = range(params['patches'])
                                for p in patches:
                                    counter += 1
                                    part = (counter - 1) % args.parts
                                    run = part == args.nth - 1
                                    if not run:
                                        continue

                                    sp = pybola.model.single_patch_params(
                                        params, p)
                                    sp['beta_Ic_c'] = np.array([beta_C])
                                    sp['reduce_beta_I'] = np.array([red_bI])
                                    sp['reduce_beta_D'] = np.array([red_bD])
                                    _run(sp, pre, interv, output)
    else:
        ctry = args.country.lower()
        output = False if args.no_output else args.output
        run_scenario(args.seed, ctry, args.preemptive, interv, args.models, k,
                     None, output)


if __name__ == '__main__':
    # Ensure that the parent directory, which contains the pybola package, is
    # included in the module search path.
    script_dir = os.path.dirname(__file__)
    parent_dir = os.path.abspath(os.path.join(script_dir, '..'))
    has_pybola = os.path.isdir(os.path.join(parent_dir, "pybola"))
    if has_pybola and parent_dir not in sys.path:
        sys.path.insert(1, parent_dir)

    sys.exit(main())
