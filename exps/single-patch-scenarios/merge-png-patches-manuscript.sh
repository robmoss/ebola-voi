#!/bin/sh

BASENAME="$1"

if [ -z "${BASENAME}" ]; then
    echo "USAGE: $(basename $0) file-prefix"
    exit 2
fi

OUT="${BASENAME}.csv"

head -n 1 ${BASENAME}-01.csv > ${OUT}
tail -q -n +2 ${BASENAME}-??.csv >> ${OUT}
