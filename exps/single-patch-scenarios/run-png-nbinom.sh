#!/bin/sh

for DISP in 100 010 050 0.10 0.01; do
    ../../bin/scenario.py --single-patch --pr-max 0.5 --no-output --models 500 --country png --disp $DISP > png-nbinom-disp-$DISP.csv
done
