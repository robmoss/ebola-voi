#!/usr/bin/Rscript --vanilla

suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(themergm))
suppressPackageStartupMessages(library(Cairo))

load_from_csv <- function(csv_file) {
    df0 <- read.csv(csv_file, header = TRUE, stringsAsFactors = FALSE)

    df0$Popn <- 'Rural'
    df0$Popn[df0$Region == 'Port Moresby'] <- 'Urban'

    df <- df0[((df0$Popn == 'Urban' & df0$BetaC > 0.1) |
               (df0$Popn == 'Rural' & df0$BetaC == 0.1)), ]

    outcomes <- c('PrDieOut', 'PrSmall', 'PrLarge')
    df1 <- reshape(df, varying = outcomes, times = outcomes,
                   v.names = 'Pr', timevar = 'Outcome',
                   direction = 'long')

    df1$Region <- factor(df1$Region, ordered = TRUE,
                         levels = c('Port Moresby', 'Southern', 'Highlands',
                                    'Momase', 'Islands'))
    df1$PrCaseAsc <- 100 * df1$PrCaseAsc
    df1$Pr <- 100 * df1$Pr
    df1$Outcome <- factor(df1$Outcome, ordered = TRUE, levels = outcomes)
    df1$FD_lbl <- paste0('FD = ', df1$FD_nth)
    df1$FD_lbl <- factor(df1$FD_lbl, ordered = TRUE,
                         levels = unique(paste0('FD = ', sort(df1$FD_nth))))
    df1$Int_lbl <- factor(df1$Intervention, ordered = TRUE,
                          levels = c('None', 'Small', 'Large'))
    df1$Beta_lbl <- paste0(100 * (1 - df1$ScaleBetaI), '%')

    return(df1)
}


traffic_light_fill <- function(rev = FALSE) {
    outcomes <- c('PrDieOut', 'PrSmall', 'PrLarge')
    labels <- paste0(' ', c('Die-out', 'Controlled', 'Uncontrolled'), '    ')
    labels <- factor(labels, levels = labels, ordered = TRUE)
    values <- c("#006A00", "#FFAA00", "#FF0000")
    if (rev) {
        outcomes <- rev(outcomes)
        labels <- rev(labels)
    }
    return(scale_fill_manual("", breaks = outcomes, labels = labels,
                             values = values))
}

theme_settings <- function() {
    return(theme_rgm(axis.black = TRUE))
}

plot_fd_vs_intv <- function(df, rgn = 'Port Moresby') {
    pdf <- df[df$Region == rgn, ]

    p <- ggplot(pdf, aes(x = PrCaseAsc, y = Pr, fill = Outcome)) +
        geom_bar(stat = 'identity') +
        traffic_light_fill() +
        xlab('Case Ascertainment (%)') +
        ylab('Outcome Frequency (%)') +
        scale_y_continuous(breaks = c(0, 50, 100)) +
        facet_grid(Int_lbl ~ FD_lbl) +
        theme_settings() +
        theme(legend.position = 'top')

    return(p)
}

plot_fd_vs_beta <- function(df, rgn = 'Port Moresby', intv = 'None') {
    pdf <- df[df$Region == rgn & df$Intervention == intv, ]

    p <- ggplot(pdf, aes(x = PrCaseAsc, y = Pr, fill = Outcome)) +
        geom_bar(stat = 'identity') +
        traffic_light_fill() +
        xlab('Case Ascertainment (%)') +
        ylab('Outcome Frequency (%)') +
        scale_y_continuous(breaks = c(0, 50, 100)) +
        facet_grid(Beta_lbl ~ FD_lbl) +
        theme_settings() +
        theme(legend.position = 'top')

    return(p)
}

plot_fd_vs_decr <- function(df, rgn = 'Port Moresby', scale = 0.75) {
    pdf <- df[df$Region == rgn
              & ((df$ScaleBetaI == 1 & df$ScaleBetaD == 1) |
                 (df$ScaleBetaI == scale & df$ScaleBetaD == 1) |
                 (df$ScaleBetaI == 1 & df$ScaleBetaD == scale) |
                 (df$ScaleBetaI == scale & df$ScaleBetaD == scale)), ]
    pdf <- pdf[pdf$Intervention == 'None', ]

    pdf$Scale_lbl <- 'Baseline'
    pdf$Scale_lbl[pdf$ScaleBetaI < 1 & pdf$ScaleBetaD == 1] <- 'Community'
    pdf$Scale_lbl[pdf$ScaleBetaD < 1 & pdf$ScaleBetaI == 1] <- 'Dead bodies'
    pdf$Scale_lbl[pdf$ScaleBetaD < 1 & pdf$ScaleBetaI < 1] <- 'Both'
    pdf$Scale_lbl <- factor(pdf$Scale_lbl, ordered = TRUE,
                            levels = c('Baseline', 'Community',
                                       'Dead bodies', 'Both'))

    p <- ggplot(pdf, aes(x = PrCaseAsc, y = Pr, fill = Outcome)) +
        geom_bar(stat = 'identity') +
        traffic_light_fill() +
        xlab('Case Ascertainment (%)') +
        ylab('Outcome Frequency (%)') +
        scale_y_continuous(breaks = c(0, 50, 100)) +
        facet_grid(Scale_lbl ~ FD_lbl) +
        theme_settings() +
        theme(legend.position = 'top')

    return(p)
}

plot_patch_interventions <- function(df, region) {
    plot_df <- df[df$Region == region, ]
    plot_df <- plot_df[plot_df$PrCaseAsc <= 50, ]

    p <- ggplot(plot_df, aes(x = PrCaseAsc, y = Pr, fill = Outcome)) +
        geom_bar(stat = 'identity') +
        traffic_light_fill() +
        xlab('Case Ascertainment (%)') +
        ylab('Outcome Frequency (%)') +
        scale_x_continuous(breaks = c(10, 20, 30, 40, 50)) +
        scale_y_continuous(breaks = c(0, 50, 100)) +
        facet_grid(Int_lbl ~ FD_lbl) +
        theme_settings() +
        theme(legend.position = 'bottom') +
        ggtitle(region)

    return(p)
}

df <- load_from_csv('png-interventions.csv')
df <- df[df$Popn == 'Rural' | df$BetaC == 0.2, ]
df <- df[df$ScaleBetaI == 1 & df$ScaleBetaD == 1, ]
regions <- sort(unique(df$Region))

CairoPDF('png-patch-interventions.pdf', bg = 'transparent',
         width = 9.8, height = 8.0)
for (region in regions) {
    print(plot_patch_interventions(df, region))
}
invisible(dev.off())
