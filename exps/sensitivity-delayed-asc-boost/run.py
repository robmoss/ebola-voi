#!/usr/bin/env python
"""
This script runs a number of simulations to explore the sensitivity of a
single-patch model to health care system size, case ascertainment, time of
first detection, and changes to social behaviours.
"""

from __future__ import absolute_import, division, print_function
from __future__ import unicode_literals

import argparse
import numpy as np
import os
import sys


def outcomes(params, sv_table, baseline_die_pcnt=26.0):
    """
    The baseline percentage of simulations that result in die-out was
    determined by simulating with no response (set detect_nth = N + 1).
    """
    # Determine the index of the first row for the final time-step.
    from_ix = - params['models']
    last_row = sv_table[from_ix:]

    # Calculate the net cases and final attack rates.
    net_cases = last_row['CumI']
    final_AR = net_cases / params['N']

    num_small = np.sum(final_AR <= 0.01)
    num_large = np.sum(final_AR > 0.01)

    pcnt_small = 100 * num_small / params['models']
    pcnt_large = 100 * num_large / params['models']

    if pcnt_small >= baseline_die_pcnt:
        pcnt_die = baseline_die_pcnt
        pcnt_ctrl = pcnt_small - baseline_die_pcnt
    else:
        pcnt_die = pcnt_small
        pcnt_ctrl = 0

    denom = pcnt_ctrl + pcnt_large
    rel_ctrl = 100 * pcnt_ctrl / denom
    rel_large = 100 * pcnt_large / denom

    return {'size': net_cases, 'AR': final_AR,
            'die': pcnt_die, 'ctl': pcnt_ctrl, 'unc': pcnt_large,
            'rel_ctl': rel_ctrl, 'rel_unc': rel_large}


def run(opts):
    import pybola.model
    import pybola.sim

    num_parts = opts.parts
    this_part = opts.this % num_parts
    if opts.out is None:
        out_file = sys.stdout
    else:
        out_file = opts.out

    # Define the population size, keep it reasonably small.
    N = 1e5
    params = pybola.model.params(N)

    # Set the number of models to run in parallel.
    params['models'] = int(opts.models)

    # Only print column headers for the first output file.
    if this_part == 1 % num_parts:
        print("nth boosted_pr delay outcome fraction", file=out_file)

    row = "{} {} {} {} {:0.2f}"

    frac_hcw = 0.001
    base_pr = 0.2
    params['max_bed'][:] = N * frac_hcw / 15
    params['max_ct'][:] = N * frac_hcw / 50
    params['pr_detect_int'][:] = base_pr
    params['reduce_beta_D'][:] = 1.0

    counter = 1
    for nth in [5, 10, 25, 50]:
        params['detect_nth'][:] = nth
        for boost_by in [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]:
            params['extra_pr_asc'][:] = boost_by
            boost_to = boost_by + base_pr
            for delay in range(9):
                params['extra_delay'][:] = 8 * delay

                run_sim = counter % num_parts == this_part
                if run_sim:
                    # Simulate the scenario and collect the results.
                    sv_table = pybola.sim.run(params)
                    res = outcomes(params, sv_table)

                    for k in ['die', 'ctl', 'unc', 'rel_ctl', 'rel_unc']:
                        print(row.format(nth, boost_to, delay, k, res[k]),
                              file=out_file)

            counter += 1


def get_parser():
    p = argparse.ArgumentParser()

    p.add_argument(
        '-m', '--models', default=1000, type=int,
        help='The number of model realisations per scenario (default: 1000)')
    p.add_argument(
        '-o', '--out', type=argparse.FileType('w'), default=None,
        help='The output file (default: stdout)')
    p.add_argument(
        '-d', '--dir', default='.',
        help='The directory for additional output files (default: .)')
    p.add_argument(
        'parts', type=int, default=1,
        help='The number of simulation partitions (default: 1)')
    p.add_argument(
        'this', type=int, default=1,
        help='The partition to simulate (default: 1)')

    return p


def main(args=None):
    opts = get_parser().parse_args(args)
    run(opts)


if __name__ == '__main__':
    # Ensure that the pybola package directory is in the module search path.
    script_dir = os.path.dirname(__file__)
    root_dir = os.path.abspath(os.path.join(script_dir, '..', '..'))
    has_pybola = os.path.isdir(os.path.join(root_dir, "pybola"))
    if has_pybola and root_dir not in sys.path:
        sys.path.insert(1, root_dir)

    sys.exit(main())
