#!/usr/bin/env python
"""
This script runs many realisations of the no-response scenario, to obtain the
final-size distribution when there is no health care intervention.
"""

from __future__ import absolute_import, division, print_function
from __future__ import unicode_literals

import argparse
import numpy as np
import os
import sys


def final_cases_and_AR(params, sv_table):
    # Determine the index of the first row for the final time-step.
    from_ix = - params['models']
    last_row = sv_table[from_ix:]

    # Calculate the net cases and final attack rates.
    net_cases = last_row['CumI']
    final_AR = net_cases / params['N']

    return net_cases, final_AR


def run(opts):
    import pybola.model
    import pybola.sim

    if opts.out is None:
        out_file = sys.stdout
    else:
        out_file = opts.out

    # Define the population size, keep it reasonably small.
    N = 1e5
    params = pybola.model.params(N)

    # Set the number of models to run in parallel.
    params['models'] = int(opts.models)

    # Print column headers.
    print("outcome fraction", file=out_file)

    row_fmt = "{} {:0.1f}"
    f_cases = os.path.join(opts.dir, 'baseline-net-cases.ssv')
    f_ar = os.path.join(opts.dir, 'baseline-attack-rates.ssv')

    # Disable interventions.
    params['detect_nth'][:] = N + 1

    # Simulate the scenario and collect the results.
    sv_table = pybola.sim.run(params)
    # Calculate the epidemic outcomes.
    die, ctl, unc = pybola.sim.outcomes(params, sv_table)
    # Convert the outcomes from fractions to percentages.
    die, ctl, unc = 100 * die, 100 * ctl, 100 * unc

    print(row_fmt.format('die', die), file=out_file)
    print(row_fmt.format('ctl', ctl), file=out_file)
    print(row_fmt.format('unc', unc), file=out_file)

    # Save the final size to disk.
    net_cases, final_AR = final_cases_and_AR(params, sv_table)
    net_cases.tofile(f_cases, sep='\n')
    final_AR.tofile(f_ar, sep='\n')


def get_parser():
    p = argparse.ArgumentParser()

    p.add_argument(
        '-m', '--models', default=1000, type=int,
        help='The number of model realisations per scenario (default: 1000)')
    p.add_argument(
        '-o', '--out', type=argparse.FileType('w'), default=None,
        help='The output file (default: stdout)')
    p.add_argument(
        '-d', '--dir', default='.',
        help='The directory for additional output files (default: .)')

    return p


def main(args=None):
    opts = get_parser().parse_args(args)
    run(opts)


if __name__ == '__main__':
    # Ensure that the pybola package directory is in the module search path.
    script_dir = os.path.dirname(__file__)
    root_dir = os.path.abspath(os.path.join(script_dir, '..', '..'))
    has_pybola = os.path.isdir(os.path.join(root_dir, "pybola"))
    if has_pybola and root_dir not in sys.path:
        sys.path.insert(1, root_dir)

    sys.exit(main())
